Uses WorkWithMatrix;

var
  Matrix: RealMatrix;

Begin
  CreateMatrix(Matrix);
  WriteLn('The initial matrix: ');
  PrintMatrix(Matrix);
  Write('Quantity positive elements to second diagonale: ');
  WriteLn(CountPositiveElementsSecondDiagonale(Matrix));
  ReplaceNegativeElementsBelowMainDiagonaleToNull(Matrix);
  WriteLn('Modified matrix: ');
  PrintMatrix(Matrix);
  ReadLn;
End.
