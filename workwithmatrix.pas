unit WorkWithMatrix;

interface
const
  size=9;
  MaxValue=23;
  MinValue=-21;

type
  RealMatrix = array [1..size,1..size] of real;

function GetRandom():real;
procedure CreateMatrix(var Matrix: RealMatrix);
procedure PrintMatrix(var Matrix: RealMatrix);
procedure ReplaceNegativeElementsBelowMainDiagonaleToNull(var Matrix: RealMatrix);
function CountPositiveElementsSecondDiagonale(var Matrix: RealMatrix): integer;

implementation

function GetRandom(): real;
begin
  GetRandom := (random * (MaxValue-MinValue)) + MinValue;
end;

procedure CreateMatrix(var Matrix: RealMatrix);
var
  i,j: byte;
begin
  for i:=1 to size do
  begin
    for j:=1 to size do
    begin
      Matrix[i,j] := GetRandom();
    end;
  end;
end;

procedure PrintMatrix(var Matrix: RealMatrix);
var
  i,j: byte;
begin
  for i:=1 to size do
  begin
    for j:=1 to size do
    begin
      write (Matrix[i,j]:8:2);
    end;
      writeln;
  end;
  writeln;
end;

procedure ReplaceNegativeElementsBelowMainDiagonaleToNull(var Matrix: RealMatrix);
var
  i, j:byte;
begin
  for i:=2 to size do
  begin
    for j:=1 to size-1 do
    begin
      if i>j then
      begin
        if Matrix[i,j]<0 then
        begin
         Matrix[i,j]:=0;
        end;
      end;
    end;
  end;
end;

function CountPositiveElementsSecondDiagonale(var Matrix: RealMatrix): integer;
var
  i, j, PositiveQuantity: integer;
begin
  PositiveQuantity := 0;
  for i:=1 to size do
  begin
      if (Matrix[i,size-i+1]>0) then
      begin
        PositiveQuantity += 1;
      end;
  end;
     CountPositiveElementsSecondDiagonale := PositiveQuantity;
end;

end.
